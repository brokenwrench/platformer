﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

    public Transform groundCheck;
    public float groundCheckRadius;
    public LayerMask isGround;
	private bool onGround;
	float oldVel;
	float newVel;

    public Rigidbody2D rb;

	// Use this for initialization
	void Start () {

        rb = GetComponent<Rigidbody2D>();

	}
	
	// Update is called once per frame
 	void Update () {

	}

    void FixedUpdate()
    {
		rb.velocity = new Vector2(3, rb.velocity.y);
		//if (rb.velocity.y < 1f && onGround == true)

		/*This if-statement prevents Player from halting suddenly.
		 *If player is touching the ground, a small amount of upward
		 *velocity is applied, preventing them from getting stuck on
		 *the tiles.
		 *The first half of the if-statement is necessary because
		 *without it, the player could only jump like 1/4 his normal 
		 *height. The first condition means the upward velocity is only
		 *applied if the player isn't trying to jump.
		 *The glitch hasn't gone away completely, but this has reduced
		 *occurrances so it only has a chance of occuring after a player jumps
		 *
		 */
		if (!Input.GetKey(KeyCode.Space) && onGround == true)
			rb.velocity = new Vector2 (rb.velocity.x, 1.7f);

        onGround = Physics2D.OverlapCircle(groundCheck.position, groundCheckRadius, isGround);

        if (Input.GetKey(KeyCode.Space) && onGround == true)
        {

            rb.velocity = new Vector2(rb.velocity.x, 20);

        }

                
    }

	void OnTriggerEnter2D(Collider2D collision)
	{

		if (collision.gameObject.tag == "ground")
		{

			Destroy(gameObject);

		}

	}

}
